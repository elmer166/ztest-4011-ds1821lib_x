/*! \file read1821.c
 *
 *  \brief Read a byte from the DS1821 digital thermometer
 *
 *
 *  \author jjmcd
 *  \date December 22, 2012
 */

#include <xc.h>
#include "DS1821.h"

/*! Read a byte from the DS1821 */
unsigned char read1821( void )
{
    int i,result;

    result = 0;
    for ( i=0; i<8; i++ )
    {
        /* Move to the next bit */
        result = result >> 1;

        /*! Send a read pulse to the DS1821 */
        TRIS1821 = 0;
        LAT1821 = 0;
        delay1821( T1US );

        LAT1821 = 1;
        TRIS1821 = 1;

        /* Reset the timer - The read time slot is 60us, however, the 1821
         * will respond between 1 and 15 us, so we will watch the timer, sample
         * at 7us, and then allow the full 60us to expire before moving on  */
        PR1821 = T60US;
        TMR1821 = 0;
        IF1821 = 0;

        /* Wait a bit for the DS1821 to respond */
        while (TMR1821 < T7US)
            ;

        /* Now sample the DS1821 */
        if (PORT1821)
            result |= 0x80;

        /* Wait for the time slot to expire */
        while ( !IF1821 )
            ;

    }
    return (unsigned char)result;
}
