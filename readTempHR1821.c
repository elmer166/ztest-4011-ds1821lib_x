/*! \file readTempHR1821.c
 *
 *  \brief Read high-resolution temperature from DS821
 *
 *
 *  \author jjmcd
 *  \date December 25, 2012
 */

#include <xc.h>
#include "DS1821.h"
#include "../4011-LCDlib-D.X/delay.h"

/* Return the high resolution temperature from the DS1821 */
double readTempHR1821( void )
{
    int temperature;
    double fTemp;
    int CountPerC,CountRemain;
    int saveTRIS;

    saveTRIS=TRIS1821;
    TRIS1821=1;
    Delay(3*Delay_15mS_Cnt);

    temperature = readTemp1821();
    masterReset1821();
    command1821( DS_READCOUNTER );
    CountRemain = (int) read1821_9();
    masterReset1821();
    command1821( DS_LOADCOUNTER );
    masterReset1821();
    command1821( DS_READCOUNTER );
    CountPerC = (int) read1821_9();

    fTemp = (double)temperature - 0.5 +
            (double)(CountPerC-CountRemain)/(double)CountPerC;

    TRIS1821=saveTRIS;
    return fTemp;
}
