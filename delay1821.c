/*! \file delay1821.c
 *
 *  \brief Delay a specified amount of time based on timer
 *
 *
 *  \author jjmcd
 *  \date December 22, 2012
 */

#include <xc.h>
#include "DS1821.h"

/*! Delay a specified amount of time based on the timer */
void delay1821( int count )
{
    PR1821 = count;
    TMR1821 = 0;
    IF1821 = 0;
    while ( !IF1821 )
        ;

}