/*! \file initialize1821.c
 *
 *  \brief Initiaize the timer used by the DS1821
 *
 *
 *  \author jjmcd
 *  \date December 22, 2012
 */

#include <xc.h>
#include "DS1821.h"

/*! Initializes the timer used by the DS1821 */
void initialize1821( void )
{
    /* Set up timer for DS1821                                              */
    TMR1821 = 0;                /* Clear timer                              */
    PR1821  = 0xffff;           /* Timer counter to a large number          */
    CON1821 = 0x8000;           /* Fosc/4, 1:1 prescale, start timer        */
}


