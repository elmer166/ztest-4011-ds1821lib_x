/*! \file command1821.c
 *
 *  \brief Send a command byte to the DS1821
 *
 *
 *  \author jjmcd
 *  \date December 22, 2012
 */

#include <xc.h>
#include "DS1821.h"


/*! Send a command to the DS1821 */
void command1821( unsigned char command )
{
    int i;

    for ( i=0; i<8; i++ )
    {
        /* Reset the timer */
        PR1821 = T60US;
        TMR1821 = 0;
        IF1821 = 0;

        /* Pull down the pin */
        TRIS1821 = 0;
        LAT1821 = 0;

        /* Wait for 7 us to expire */
        /* Master must keep bus low for>1us and <15us */
        while ( TMR1821<T7US )
            ;

        /* if this bit is a 1, raise the DQ pin */
        if ( command & 0x01 )
            LAT1821 = 1;

        /* Wait for the time slot to expire */
        while ( !IF1821 )
            ;

        /* Reset the bus */
        LAT1821 = 1;
        TRIS1821 = 1;

        /* Next bit */
        command = command >> 1;
    }
    /* Wait 1 microcsecond */
    delay1821( T1US );

}
