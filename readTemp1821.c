/*! \file readTemp1821.c
 *
 *  \brief Read the DS1821 digital thermometer
 *
 *
 *  \author jjmcd
 *  \date December 22, 2012
 */

#include <xc.h>
#include "DS1821.h"
#include "../4011-LCDlib-D.X/delay.h"

/*! Get the current temperature from the DS1821 */
unsigned char readTemp1821( void )
{
    unsigned char result;
    int saveTRIS;

    saveTRIS=TRIS1821;
    TRIS1821=1;
    Delay( 3*Delay_15mS_Cnt);

    /* Set the DS1821 into one-shot mode */
    masterReset1821();
    command1821( DS_WRITESTATUS );
    command1821( DS_CONFIG | DS_ONESHOT );

    /* Instruct the DS1821 to begin a temperature conversion */
    masterReset1821();
    command1821( DS_STARTCONV );

    /* Wait for the conversion to complete */
    pollStatus1821();

    /* Send read temperature command */
    masterReset1821();
    command1821( DS_READTEMP );

    /* Read the result */
    result = read1821();

    TRIS1821=saveTRIS;
    return result;
}

