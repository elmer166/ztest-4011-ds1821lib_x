/*! \file masterReset1821.c
 *
 *  \brief Reset the DS1821
 *
 *  Sends a reset pulse to the DS1821 Programmable Digital Thermostat
 *  and Thermometer and waits for the presence pulse to complete.
 *
 *  \author jjmcd
 *  \date December 22, 2012
 */

#include <xc.h>
#include "DS1821.h"


/*! Send the master reset to the DS1821 */
void masterReset1821()
{
    /* Send the reset pulse */
    TRIS1821 = 0;           /* Make pin output */
    LAT1821 = 0;            /* Set it low */
    /* Keep it low for half a millisecond */
    delay1821( T500US );

    LAT1821 = 1;            /* Now make the pin high */
    TRIS1821 = 1;           /* And make it an input again */

    /* Wait for the presence pulse*/
    /* First, give it a microsecond */
    delay1821( T1US );
    while( PORT1821 ) ;     /* Wait for pulse to start */
    while( !PORT1821 ) ;    /* Wait for pulse to end */
}
