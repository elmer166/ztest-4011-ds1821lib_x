/*! \file DS1821.h
 *
 *  \brief Declarations for Dallas DS1821 temperature sensor
 *
 *
 *  \author jjmcd
 *  \date December 22, 2012, 11:43 AM
 */

#ifndef DS1821_H
#define	DS1821_H

#ifdef	__cplusplus
extern "C" {
#endif

/* The following define the pin to which the DS1821 is connected */
/*! Data direction pin for the DS1821 */
#define TRIS1821 _TRISE8
/*! Output latch for the DS1821 */
#define LAT1821  _LATE8
/*! Port bit for the DS1821 */
#define PORT1821 _RE8

/* The following define the timer to be used */
/*! Timer counter */
#define TMR1821    TMR4
/*! Timer PR register */
#define PR1821    PR4
/*! Timer control register */
#define CON1821  T4CON
/*! Timer interrupt flag */
#define IF1821  _T4IF

/* Timer settings for various times based on 29.4MHz Fcy */
/*! 1 microsecond count */
#define T1US   30
/*! 7 microsecond count */
#define T7US   207
/*! 15 microsecond count */
#define T15US  441
/*! 60 microsecond count */
#define T60US  1770
/*! 500 microsecond count */
#define T500US 14746

/*! DS1821 command to accept status */
#define DS_WRITESTATUS  0x0c
/*! DS1821 command to start temperature conversion */
#define DS_STARTCONV    0xee
/*! DS1821 command to read the temperature */
#define DS_READTEMP     0xaa
/*! DS1821 command to send its status */
#define DS_READSTATUS   0xac
/*! DS1821 command to send counter contents */
#define DS_READCOUNTER   0xa0
/*! DS1821 command to load slope accumulator to counter */
#define DS_LOADCOUNTER   0x41
/*! DS1821 command to accept configuration */
#define DS_CONFIG       0x40
/*! DS1821 command to enter one shot mode */
#define DS_ONESHOT      0x01
/*! DS1821 completion bit */
#define DS_DONE         0x80

/*! Initializes the timer used by the DS1821 */
void initialize1821( void );
/*! Delay a specified amount of time based on the timer */
void delay1821( int );
/*! Send the master reset to the DS1821 */
void masterReset1821( void );
/*! Send a command to the DS1821 */
void command1821( unsigned char );
/*! Wait for the DS1821 to become ready */
void pollStatus1821( void );
/*! Read a byte from the DS1821 */
unsigned char read1821( void );
/*! Get the current temperature from the DS1821 */
unsigned char readTemp1821( void );
/*! Read a 9 bit word from the 1821*/
int read1821_9( void );
/* Return the high resolution temperature from the DS1821 */
double readTempHR1821( void );

#ifdef	__cplusplus
}
#endif

#endif	/* DS1821_H */

