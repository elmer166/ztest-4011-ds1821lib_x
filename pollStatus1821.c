/*! \file pollStatus1821.c
 *
 *  \brief Wait for the 1821 to report it is ready
 *
 *
 *  \author jjmcd
 *  \date December 22, 2012
 */

#include <xc.h>
#include "DS1821.h"


/*! Wait for the DS1821 to become ready */
void pollStatus1821( void )
{
    int done;

    done = 0;
    while ( !done )
    {
        masterReset1821();
        command1821( DS_READSTATUS );
        if ( read1821() & DS_DONE )
            done = 1;
    }
}

