/*! \file read1821_9.c
 *
 *  \brief Read a 9 bit value from the DS1829
 *
 *
 *  \author jjmcd
 *  \date December 25, 2012
 */

#include <xc.h>
#include "DS1821.h"


/*! Read a 9 bit word from the 1821*/
int read1821_9( void )
{
    int i,result;

    result = 0;
    for ( i=0; i<9; i++ )
    {
        /* Move to the next bit */
        result = result >> 1;

        /*! Send a read pulse to the DS1821 */
        TRIS1821 = 0;
        LAT1821 = 0;
        delay1821( T1US );

        LAT1821 = 1;
        TRIS1821 = 1;

        /* Reset the timer - The read time slot is 60us, however, the 1821
         * will respond between 1 and 15 us, so we will watch the timer, sample
         * at 7us, and then allow the full 60us to expire before moving on  */
        PR1821 = T60US;
        TMR1821 = 0;
        IF1821 = 0;

        /* Wait a bit for the DS1821 to respond */
        while (TMR1821 < T7US)
            ;

        /* Now sample the DS1821 */
        if (PORT1821)
            result |= 0x80;

        /* Wait for the time slot to expire */
        while ( !IF1821 )
            ;

    }
    return (int)result;
}

